#include "SingleBTagConfig.hh"
#include "ConfigFileTools.hh"

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/filesystem.hpp>

namespace {
  TrackSortOrder get_sort_order(const boost::property_tree::ptree& pt,
                                const std::string key) {
    std::string val = pt.get<std::string>(key);
    if (val == "abs_d0_significance") {
      return TrackSortOrder::ABS_D0_SIGNIFICANCE;
    }
    if (val == "abs_d0") {
      return TrackSortOrder::ABS_D0;
    }
    if (val == "d0_significance") {
      return TrackSortOrder::D0_SIGNIFICANCE;
    }
    throw std::logic_error("sort order '" + val + "' not recognized");
  }
}

SingleBTagConfig get_singlebtag_config(const std::string& config_file_name) {
  namespace fs = boost::filesystem;
  namespace pt = boost::property_tree;
  namespace cft = ConfigFileTools;
  SingleBTagConfig config;

  pt::ptree cfg;
  pt::read_json(config_file_name, cfg);

  config.do_calibration = cft::boolinate(cfg, "do_calibration");
  config.run_augmenters = cft::boolinate(cfg, "run_augmenters");
  config.vr_cuts = cft::boolinate(cfg, "vr_cuts");
  config.jet_collection = cfg.get<std::string>("jet_collection");

  if(config.do_calibration){
  config.jet_calib_file = cfg.get<std::string>("jet_calib_file");
  config.cal_seq = cfg.get<std::string>("cal_seq");
  config.cal_area = cfg.get<std::string>("cal_area");
  config.jvt_cut = cfg.get<float>("jvt_cut");
  }

  config.pt_cut = cfg.get<float>("pt_cut");

  config.n_tracks_to_save = cfg.get<size_t>("n_tracks_to_save");
  config.track_sort_order = get_sort_order(cfg,"track_sort_order");

  // optional configuration
  for (const auto& paths: cfg.get_child("nn_file_paths")) {
    config.nn_file_paths.push_back(paths.second.get_value<std::string>());
  }

  // combine variable files if there are any file paths specified
  for (auto& node: cfg.get_child("variables")) {
    cft::combine_files(node.second, fs::path(config_file_name));
  }

  config.btag = cft::get_variable_list(cfg.get_child("variables.btag"));
  config.track = cft::get_variable_list(cfg.get_child("variables.track"));

  return config;
}
