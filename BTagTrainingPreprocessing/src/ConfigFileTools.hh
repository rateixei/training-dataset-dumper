#ifndef CONFIG_FILE_TOOLS_HH
#define CONFIG_FILE_TOOLS_HH

#include <boost/property_tree/ptree.hpp>
#include <boost/filesystem.hpp>
#include <string>
#include <vector>
#include <map>

namespace ConfigFileTools {

  typedef std::map<std::string,std::vector<std::string>> MapOfLists;

  MapOfLists get_variable_list(const boost::property_tree::ptree& pt);
  bool boolinate(const boost::property_tree::ptree& pt,
                 const std::string key);
  void combine_files(boost::property_tree::ptree& node,
                     boost::filesystem::path config_path);
}

#endif
