#ifndef JET_CONST_WRITER_CONFIG_HH
#define JET_CONST_WRITER_CONFIG_HH

#include <string>
#include <vector>

struct JetConstWriterConfig {
  std::string name;

  std::vector<std::size_t> output_size;

  std::vector<std::string> uchar_variables;
  std::vector<std::string> int_variables;
  std::vector<std::string> float_variables;
  std::vector<std::string> double_variables;
};


#endif
