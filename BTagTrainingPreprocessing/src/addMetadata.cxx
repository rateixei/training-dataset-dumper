#include "addMetadata.hh"

#include "H5Cpp.h"
#include "BookKeeper.hh"

#include "HDF5Utils/Writer.h"


void addMetadata(H5::Group& grp, const Counts& counts){
  H5Utils::Consumers<const Counts&> cons;
#define ADD(NAME, TYPE)\
  cons.add<TYPE>(#NAME,[](const Counts& c) {return c.NAME;})
  ADD(nEventsProcessed, long long);
  ADD(sumOfWeights, double);
  ADD(sumOfWeightsSquared, double);
  ADD(nIncomplete, int);
#undef ADD
  H5Utils::Writer<0, const Counts&> writer(grp, "metadata", cons);
  writer.fill(counts);
}

