#!/usr/bin/env bash

# Input derivation I got from rachael
INPUT_FILE=/afs/cern.ch/work/d/dguest/public/rachael/DAOD_FTAG2.ttbar_mc16.pool.root

# if there's no local file consider downloading one with curl
if [[ ! -f $INPUT_FILE ]] ; then
    INPUT_FILE=test_FTAG1.pool.root
    if [[ ! -f $INPUT_FILE ]] ; then
        curl http://dguest-ci.web.cern.ch/dguest-ci/ftag/dumper/DAOD_FTAG1.art.pool.root > ${INPUT_FILE}
    fi
fi

if [[ ! -f $INPUT_FILE ]] ; then
    echo "ERROR: input file $INPUT_FILE not found" >&2
    exit 1
fi

# run the script
CFG_FILE=configs/single-b-tag/EMPFlow_IPRNN.json
dump-single-btag -c $(dirname ${BASH_SOURCE[0]})/${CFG_FILE} $INPUT_FILE
